Contributing to BuildStream Docker Images
=========================================

First, thank you very much for choosing to contribute to this project. In
this document, you will find instructions about how to build and test these
Docker images as well as details about the automations surrounding them.


Pre-requisites
--------------

* [Docker][docker-link]: We are currently using `docker` to build
  our images, so you will need to have that installed in order to build things
  locally.

* [Make][gnu-make-link]: This project uses `make` to automate some of the
  mundane tasks involved with managing Docker images.

* If you want to run the linters locally, you will also need:

  - [shellcheck][shellcheck-link]: `shellcheck` is used to lint shell scripts.

  - [hadolint][hadolint-link]: `hadolint` is used to lint Dockerfiles.

  - [yamllint][yamllint-link]: `yamllint` Python package is used to lint all
    the YAML files.

  - [black][black-link]: `black` Python package is used to lint (and format)
    the python scripts.

* This repository uses [jinja2][jinja2-link] to render some files based on
  templates. If you plan on editing any of those files (more details below),
  you will also need to have the following python packages installed:

  - [jinja2][jinja2-link]
  - [pyyaml][pyyaml-link]


Variables
---------

Before we get into building the images, getting familiarized with some of the
common variables might be useful. Here, we are referring to make variables
that can be set as environment variables, or passed to make on the command
line.

* `FIXED_TAG`: This is a required variable for most of the targets related to
  building or testing the Docker images. It denotes the tag of the image that
  we want to build (or test). Under the context of CI, it always refers to a
  tag that is unique to the given build. However, you can set it to any
  arbitrary value for local testing.

* `BUILD_ARGS` (optional): Use this to supply additional build arguments to the
  `docker build` command. It is set to an empty string by default.

* `CONTEXT` (optional): Use this to specify the build context for the Docker
  build. Defatuls to the current directory.

* `DOCKERFILE` (optional): Use this to specify the path to the Dockerfile.
  Defaults to `$CONTEXT/Dockerfile`.

* `MOVING_TAG`: This is a required variable for some targets. It represents
  the tags that we expect our end-users to use. Usually you won't need this
  to be set locally, unless you are debugging some CI issues.

All of the above variables can be supplied to a `make` command like so:

    MY_VARIABLE=value MY_OTHER_VARIABLE=other-value make [...]

For more details, have a look at our [Makefile](Makefile).


Build
-----

Armed with knowledge about common variables, we will now look at how to build
an image locally.

For the system images (not testsuite images), you will notice that we have
one directory per image, like `image-tools`, etc. For
building any of these images, you will simply need to specify the `CONTEXT`
variable appropriately, like:

    CONTEXT=image-tools \
        FIXED_TAG=image-tools \
        make

By default, this will install BuildStream from the master branch. If you wish
to use a different branch (like `bst-1.2`), you can additional specify the
branch name using `BUILD_ARGS` variable like so:

    BUILD_ARGS='--build-arg BRANCH=bst-1.2' \
        CONTEXT=image-tools \
        FIXED_TAG=image-tools \
        make

In case of testsuite images, all Dockerfiles are in the `testsuite` directory.
So, you will need to specify the name of the Dockerfile as well. This can be
done like so:

    CONTEXT=testsuite \
        DOCKERFILE=testsuite/debian-10.Dockerfile \
        FIXED_TAG=testsuite-debian \
        make


Test
----

Congratulations on successfully building the image. Now you should have an
image locally with the name `FIXED_TAG`. You can run it manually to perform
some sanity checks, if you want.

For testsuite images, it is also useful to run the tests of BuildStream project
to ensure that they pass, since running those tests is the primary purpose of
these images. Although these tests will automatically run on the CI, you can
also run them manually like so:

    FIXED_TAG=testsuite-debian make bst-test


Lint
----

Once you have installed all the pre-requisites mentioned above, you should be
able to run the linters locally as well. You can run all of them in a single
command like so:

    make lint

This will run `shellcheck` on shell scripts, `hadolint` on Dockerfiles,
`yamllint` on YAML files and `black` on Python scripts. Additionally, it will
also check for trailing whitespaces.

You can also run just a specific linter (like `yamllint`) like so:

    make yamllint

To find out more about linting targets, have a look at the
[Makefile](Makefile).


Format
------

In case of Python, we can do one better than linting. We use `black` not only
as a linter, but also as a tool to automatically format our code in the right
way. If you are making changes to any Python scripts, run the following
command to blacken the code:

    make black-fmt


Render
------

As we alluded to in the pre-requisites section, some of the files in this
repository are generated from templates. Hence, they should not be edited
directly and the corresponding template should be modified instead. It should
be easy to tell if you are looking at an auto-generated file as it should
contain a big "AUTOGENERATED FILE" warning at the top.

Notably, we are using the following templates:

* `templates/gitlab-ci.yml.j2`: to generate `.gitlab-ci.yml` based on details
  in `templates/variables.yml`.

* `templates/Dockerfile.j2`: to generate all `.Dockerfile`s under the
  `testsuite` directory, using `tempates/testsuite_images.yml`.

In the interest of time and space, we will not list here all the configuration
options for these templates here. But, the templates themselves should be
self-documenting.

Once you have edited one of these templates, you can run the following command
to regenerate the corresponding files:

    make render

NOTE: If you are creating a new template, please remember to include
`{% include "./autogen-warning.j2" %}` at the top.


[docker-link]: https://www.docker.com/
[gnu-make-link]: https://www.gnu.org/software/make/
[shellcheck-link]: https://github.com/koalaman/shellcheck
[hadolint-link]: https://github.com/hadolint/hadolint
[yamllint-link]: https://github.com/adrienverge/yamllint
[black-link]: https://github.com/ambv/black
[jinja2-link]: http://jinja.pocoo.org/
[pyyaml-link]: https://pyyaml.org/wiki/PyYAML
